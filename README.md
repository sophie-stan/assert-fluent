# Assert Fluent

A fluent API for assertions in Kotlin, greatly inspired by the Fluent Assertion API of [Jhispter Lite](https://github.com/jhipster/jhipster-lite/tree/main/src/main/java/tech/jhipster/lite/error/domain).

## Examples

For further understanding, see the French articles by Colin Damon:
- [Eviter les bugs et anticiper l'avenir avec des types](https://blog.ippon.fr/2022/02/18/eviter-les-bugs-et-anticiper-lavenir-avec-des-types/)
- [Des objets pas des data class](https://blog.ippon.fr/2020/04/01/des-objets-pas-des-data-classes/)
