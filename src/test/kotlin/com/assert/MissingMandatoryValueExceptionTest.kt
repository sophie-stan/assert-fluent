package com.assert

import com.assert.MissingMandatoryValueException.Companion.forBlankValue
import com.assert.MissingMandatoryValueException.Companion.forEmptyValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MissingMandatoryValueExceptionTest {
  @Test
  fun `Should get exception for blank value`() {
    val exception = forBlankValue(FIELD)

    assertThat(exception.message).isEqualTo("The field \"field\" is mandatory and wasn't set (blank)")
  }

  @Test
  fun `Should get exception for empty collection`() {
    val exception = forEmptyValue(FIELD)

    assertThat(exception.message).isEqualTo("The field \"field\" is mandatory and wasn't set (empty)")
  }

  companion object {
    const val FIELD = "field"
  }
}
