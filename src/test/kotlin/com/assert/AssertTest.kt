package com.assert

import com.assert.Assert.field
import com.assert.Assert.noWhitespace
import com.assert.Assert.notBlank
import com.assert.Assert.notEmpty
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.math.BigDecimal
import java.time.Instant
import java.util.*

internal class AssertTest {
  @Test
  fun `Should not validate empty string`() {
    assertNotBlankString("")
  }

  @Test
  fun `Should not validate space string`() {
    assertNotBlankString(" ")
  }

  @Test
  fun `Should not validate tab string`() {
    assertNotBlankString("\t")
  }

  @Test
  fun `Should not validate string with whitespace`() {
    assertThatThrownBy { noWhitespace("field", "my tag") }
      .isExactlyInstanceOf(StringWithWhitespacesException::class.java)
      .hasMessageContaining("\"field\"")
      .hasMessageContaining("contains at least one space")
  }

  @Test
  fun `Should validate string without whitespace`() {
    assertThatCode {
      noWhitespace(
        "field",
        NOT_EMPTY
      )
    }.doesNotThrowAnyException()
  }

  @Test
  fun `Should validate non blank`() {
    assertThatCode { notBlank("field", NOT_EMPTY) }.doesNotThrowAnyException()
  }

  private fun assertNotBlankString(input: String) {
    assertThatThrownBy { notBlank("field", input) }
      .isExactlyInstanceOf(MissingMandatoryValueException::class.java)
      .hasMessageContaining("\"field\"")
      .hasMessageContaining("(blank)")
  }

  @Test
  fun `Should not validate empty collection`() {
    val list = listOf<String>()
    assertThatThrownBy { notEmpty("field", list) }
      .isExactlyInstanceOf(MissingMandatoryValueException::class.java)
      .hasMessageContaining("empty")
  }

  @Test
  fun `Should validate not empty collection`() {
    assertThatCode { notEmpty("field", listOf("Hello")) }.doesNotThrowAnyException()
  }

  @Nested
  @DisplayName("String")
  internal inner class AssertStringTest {
    @Test
    fun `Should not validate blank string as not blank`() {
      assertThatThrownBy {
        field(FIELD_NAME, " ").notBlank()
      }
        .isExactlyInstanceOf(MissingMandatoryValueException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("(blank)")
    }

    @Test
    fun `Should validate string with value as not blank`() {
      assertThatCode {
        field(FIELD_NAME, "value").notBlank()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate too short string value`() {
      assertThatThrownBy {
        field(FIELD_NAME, "value").minLength(6)
      }
        .isExactlyInstanceOf(StringTooShortException::class.java)
        .hasMessageContaining(6.toString())
        .hasMessageContaining("value".length.toString())
        .hasMessageContaining(FIELD_NAME)
    }

    @ParameterizedTest
    @ValueSource(ints = [-1, 0])
    fun `Should validate zero or negative min length with string value`(minLength: Int) {
      assertThatCode {
        field(FIELD_NAME, "value").minLength(minLength)
      }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(ints = [4, 5])
    fun `Should validate long enough string`(minLength: Int) {
      assertThatCode {
        field(FIELD_NAME, "value").minLength(minLength)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate too long string value`() {
      assertThatThrownBy {
        field(FIELD_NAME, "value").maxLength(4)
      }
        .isExactlyInstanceOf(StringTooLongException::class.java)
        .hasMessageContaining(4.toString())
        .hasMessageContaining("value".length.toString())
        .hasMessageContaining(FIELD_NAME)
    }

    @ParameterizedTest
    @ValueSource(ints = [5, 6])
    fun `Should validate short enough string`(maxLength: Int) {
      assertThatCode {
        field(FIELD_NAME, "value").maxLength(maxLength)
      }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("Integer")
  internal inner class AssertIntegerTest {
    @Test
    fun `Should not validate negative value as positive`() {
      assertThatThrownBy {
        field(FIELD_NAME, -4).positive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
        .hasMessageContaining("-4")
    }

    @ParameterizedTest
    @ValueSource(ints = [0, 42])
    fun `Should validate positive values as positive`(value: Int) {
      assertThatCode {
        field(FIELD_NAME, value).positive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value under min`() {
      assertThatThrownBy { field(FIELD_NAME, 42).min(1337) }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1337")
    }

    @ParameterizedTest
    @ValueSource(ints = [41, 42])
    fun `Should validate value over min`(min: Int) {
      assertThatCode { field(FIELD_NAME, 42).min(min) }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value over max`() {
      assertThatThrownBy { field(FIELD_NAME, 42).max(12) }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("12")
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(ints = [42, 43])
    fun `Should validate value under max`(max: Int) {
      assertThatCode { field(FIELD_NAME, 42).max(max) }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("Long")
  internal inner class AssertLongTest {
    @Test
    fun `Should not validate negative value as positive`() {
      assertThatThrownBy {
        field(FIELD_NAME, -4L).positive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
        .hasMessageContaining("-4")
    }

    @ParameterizedTest
    @ValueSource(longs = [0, 42])
    fun `Should validate positive values as positive`(value: Long) {
      assertThatCode {
        field(FIELD_NAME, value).positive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value under min`() {
      assertThatThrownBy {
        field(FIELD_NAME, 42L).min(1337)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1337")
    }

    @ParameterizedTest
    @ValueSource(longs = [41, 42])
    fun `Should validate value over min`(min: Long) {
      assertThatCode { field(FIELD_NAME, 42L).min(min) }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value over max`() {
      assertThatThrownBy { field(FIELD_NAME, 42L).max(12) }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("12")
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(longs = [42, 43])
    fun `Should validate value under max`(max: Long) {
      assertThatCode { field(FIELD_NAME, 42L).max(max) }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("Float")
  internal inner class AssertFloatTest {
    @Test
    fun `Should not validate negative value as positive`() {
      assertThatThrownBy {
        field(FIELD_NAME, -4f).positive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
        .hasMessageContaining("-4")
    }

    @ParameterizedTest
    @ValueSource(floats = [0F, 42F])
    fun `Should validate positive values as positive`(value: Float) {
      assertThatCode {
        field(FIELD_NAME, value).positive()
      }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(floats = [-0.1f, 0F])
    fun `Should not validate negative and zero value as striclty positive`(value: Float) {
      assertThatThrownBy {
        field(FIELD_NAME, value).strictlyPositive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
        .hasMessageContaining(value.toString())
    }

    @ParameterizedTest
    @ValueSource(floats = [0.1f, 1F])
    fun `Should validate positive value as striclty positive`(value: Float) {
      assertThatCode {
        field(FIELD_NAME, value).strictlyPositive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value under min`() {
      assertThatThrownBy {
        field(FIELD_NAME, 42f).min(1337f)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1337")
    }

    @ParameterizedTest
    @ValueSource(floats = [41F, 42F])
    fun `Should validate value over min`(min: Float) {
      assertThatCode { field(FIELD_NAME, 42f).min(min) }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(floats = [41.9f, 42F])
    fun `Should not validate value under floor`(value: Float) {
      assertThatThrownBy {
        field(FIELD_NAME, value).over(42f)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining(value.toString())
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(floats = [42.1f, 43F])
    fun `Should validate value over floor`(value: Float) {
      assertThatCode { field(FIELD_NAME, value).over(42f) }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value over max`() {
      assertThatThrownBy { field(FIELD_NAME, 42f).max(12f) }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("12")
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(floats = [42F, 43F])
    fun `Should validate value under max`(max: Float) {
      assertThatCode { field(FIELD_NAME, 42f).max(max) }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(floats = [42F, 42.5f])
    fun `Should not validate value over ceil`(value: Float) {
      assertThatThrownBy {
        field(FIELD_NAME, value).under(42f)
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining(value.toString())
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(floats = [41F, 41.9f])
    fun `Should validate value under ceil`(value: Float) {
      assertThatCode {
        field(FIELD_NAME, value).under(42f)
      }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("Double")
  internal inner class AssertDoubleTest {
    @Test
    fun `Should not validate negative value as positive`() {
      assertThatThrownBy {
        field(FIELD_NAME, -4.0).positive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
        .hasMessageContaining("-4")
    }

    @ParameterizedTest
    @ValueSource(doubles = [0.0, 42.0])
    fun `Should validate positive values as positive`(value: Double) {
      assertThatCode {
        field(FIELD_NAME, value).positive()
      }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(doubles = [-0.1, 0.0])
    fun `Should not validate negative and zero value as striclty positive`(value: Double) {
      assertThatThrownBy {
        field(FIELD_NAME, value).strictlyPositive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
        .hasMessageContaining(value.toString())
    }

    @ParameterizedTest
    @ValueSource(doubles = [0.1, 1.0])
    fun `Should validate positive value as striclty positive`(value: Double) {
      assertThatCode {
        field(FIELD_NAME, value).strictlyPositive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value under min`() {
      assertThatThrownBy {
        field(FIELD_NAME, 42.0).min(1337.0)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1337")
    }

    @ParameterizedTest
    @ValueSource(doubles = [41.0, 42.0])
    fun `Should validate value over min`(min: Double) {
      assertThatCode { field(FIELD_NAME, 42.0).min(min) }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(doubles = [41.9, 42.0])
    fun `Should not validate value under floor`(value: Double) {
      assertThatThrownBy {
        field(FIELD_NAME, value).over(42.0)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining(value.toString())
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(doubles = [42.1, 43.0])
    fun `Should validate value over floor`(value: Double) {
      assertThatCode {
        field(FIELD_NAME, value).over(42.0)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value over max`() {
      assertThatThrownBy {
        field(FIELD_NAME, 42.0).max(12.0)
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("12")
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(doubles = [42.0, 43.0])
    fun `Should validate value under max`(max: Double) {
      assertThatCode { field(FIELD_NAME, 42.0).max(max) }.doesNotThrowAnyException()
    }

    @ParameterizedTest
    @ValueSource(doubles = [42.0, 42.5])
    fun `Should not validate value over ceil`(value: Double) {
      assertThatThrownBy {
        field(FIELD_NAME, value).under(42.0)
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining(value.toString())
        .hasMessageContaining("42")
    }

    @ParameterizedTest
    @ValueSource(doubles = [41.0, 41.9])
    fun `Should validate value under ceil`(value: Double) {
      assertThatCode {
        field(FIELD_NAME, value).under(42.0)
      }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("BigDecimal")
  internal inner class AssertBigDecimalTest {
    @Test
    fun `Should not be positive for negative value`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(-1)).positive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("-1")
        .hasMessageContaining("0")
    }

    @Test
    fun `Should be positive for zero`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal.ZERO).positive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should be positive for one`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal.ONE).positive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not be strictly positive for negative value`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(-1)).strictlyPositive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("-1")
        .hasMessageContaining("0")
    }

    @Test
    fun `Should not be strictly positive for zero`() {
      assertThatThrownBy {
        field(
          FIELD_NAME,
          BigDecimal.ZERO
        ).strictlyPositive()
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("0")
    }

    @Test
    fun `Should be strictly positive for one`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal.ONE).strictlyPositive()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate big decimal under min long value`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(1)).min(42)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should validate big decimal over min long value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42)).min(1)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should validate big decimal at min long value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42)).min(42)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate big decimal under min big decimal value`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(1)).min(BigDecimal(42))
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should validate big decimal over min big decimal value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42)).min(BigDecimal(1))
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should validate big decimal at min big decimal value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42))
          .min(BigDecimal(42))
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value over long floor`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(1)).over(42)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should not validate value at long floor`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42)).over(42)
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
    }

    @Test
    fun `Should validate value over long floor`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42)).over(1)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value under big decimal floor`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(1))
          .over(BigDecimal(42))
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should not validate value at big decimal floor`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42))
          .over(BigDecimal(42))
      }
        .isExactlyInstanceOf(NumberValueTooLowException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
    }

    @Test
    fun `Should validate value over big decimal floor`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42))
          .over(BigDecimal.ONE)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate big decimal over max long value`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42)).max(1)
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should validate big decimal under max long value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(1)).max(42)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should validate big decimal at max long value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42)).max(42)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate big decimal over max big decimal value`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42)).max(BigDecimal(1))
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should validate big decimal under max big decimal value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(1)).max(BigDecimal(1))
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should validate big decimal at max big decimal value`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(42))
          .max(BigDecimal(42))
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value under long ceil`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42)).under(1)
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should not validate value at long ceil`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42)).under(42)
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
    }

    @Test
    fun `Should validate value under long ceil`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal(1)).under(42)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate value over big decimal ceil`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42))
          .under(BigDecimal(1))
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
        .hasMessageContaining("1")
    }

    @Test
    fun `Should not validate value at big decimal ceil`() {
      assertThatThrownBy {
        field(FIELD_NAME, BigDecimal(42))
          .under(BigDecimal(42))
      }
        .isExactlyInstanceOf(NumberValueTooHighException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("42")
    }

    @Test
    fun `Should validate value over big decimal ceil`() {
      assertThatCode {
        field(FIELD_NAME, BigDecimal.ONE)
          .under(BigDecimal(42))
      }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("Collection")
  internal inner class CollectionAssertTest {
    @Test
    fun `Should not validate empty collection as not empty`() {
      assertThatThrownBy {
        field(
          FIELD_NAME,
          listOf<Any>()
        ).notEmpty()
      }
        .isExactlyInstanceOf(MissingMandatoryValueException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("(empty)")
    }

    @Test
    fun `Should validate collection with element as not empty`() {
      assertThatCode {
        field(
          FIELD_NAME,
          listOf("value")
        ).notEmpty()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate collection as negative max size`() {
      assertThatThrownBy {
        field(
          FIELD_NAME,
          listOf(
            "value1",
            "value2",
            "value3"
          )
        ).maxSize(-1)
      }
        .isExactlyInstanceOf(TooManyElementsException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("-1")
        .hasMessageContaining("3")
    }

    @Test
    fun `Should not validate collection with too many elements`() {
      assertThatThrownBy {
        field(
          FIELD_NAME,
          listOf(
            "value1",
            "value2",
            "value3"
          )
        ).maxSize(2)
      }
        .isExactlyInstanceOf(TooManyElementsException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("2")
        .hasMessageContaining("3")
    }

    @ParameterizedTest
    @ValueSource(ints = [3, 4])
    fun `Should validate collection with size under max size`(maxSize: Int) {
      assertThatCode {
        field(
          FIELD_NAME,
          listOf(
            "value1",
            "value2",
            "value3"
          )
        ).maxSize(maxSize)
      }.doesNotThrowAnyException()
    }
  }

  @Nested
  @DisplayName("Instant")
  internal inner class AssertInstantTest {
    @Test
    fun `Should not validate future instant as past`() {
      assertThatThrownBy {
        field(FIELD_NAME, future()).inPast()
      }
        .isExactlyInstanceOf(NotBeforeTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
    }

    @Test
    fun `Should validate past date as past`() {
      assertThatCode { field(FIELD_NAME, past()).inPast() }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate past instant as future`() {
      assertThatThrownBy {
        field(FIELD_NAME, past()).inFuture()
      }
        .isExactlyInstanceOf(NotAfterTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
    }

    @Test
    fun `Should validate future date as future`() {
      assertThatCode {
        field(FIELD_NAME, future()).inFuture()
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate same instant as after instant`() {
      val date = past()
      assertThatThrownBy {
        field(FIELD_NAME, date).after(date)
      }
        .isExactlyInstanceOf(NotAfterTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("strictly")
    }

    @Test
    fun `Should not validate past instant as after future instant`() {
      assertThatThrownBy {
        field(FIELD_NAME, past()).after(future())
      }
        .isExactlyInstanceOf(NotAfterTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("strictly")
    }

    @Test
    fun `Should validate future instant as after past instant`() {
      assertThatCode {
        field(FIELD_NAME, future()).after(past())
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate past instant as after or at future instant`() {
      assertThatThrownBy {
        field(FIELD_NAME, past()).afterOrAt(future())
      }
        .isExactlyInstanceOf(NotAfterTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageNotContaining("strictly")
    }

    @Test
    fun `Should validate future instant as after or at past instant`() {
      assertThatCode {
        field(FIELD_NAME, future()).afterOrAt(past())
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should validate same instant as after or at instant`() {
      val date = past()
      assertThatCode {
        field(FIELD_NAME, date).afterOrAt(date)
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate same instant as before instant`() {
      val date = past()
      assertThatThrownBy {
        field(FIELD_NAME, date).before(date)
      }
        .isExactlyInstanceOf(NotBeforeTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("strictly")
    }

    @Test
    fun `Should not validate future instant as before past instant`() {
      assertThatThrownBy {
        field(FIELD_NAME, future()).before(past())
      }
        .isExactlyInstanceOf(NotBeforeTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageContaining("strictly")
    }

    @Test
    fun `Should validate past instant as before future instant`() {
      assertThatCode {
        field(FIELD_NAME, past()).before(future())
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should not validate future instant as before or at past instant`() {
      assertThatThrownBy {
        field(FIELD_NAME, future()).beforeOrAt(past())
      }
        .isExactlyInstanceOf(NotBeforeTimeException::class.java)
        .hasMessageContaining(FIELD_NAME)
        .hasMessageNotContaining("strictly")
    }

    @Test
    fun `Should validate past instant as after or at future instant`() {
      assertThatCode {
        field(FIELD_NAME, past()).beforeOrAt(future())
      }.doesNotThrowAnyException()
    }

    @Test
    fun `Should validate same instant as before or at instant`() {
      val date = past()
      assertThatCode {
        field(FIELD_NAME, date).beforeOrAt(date)
      }.doesNotThrowAnyException()
    }

    private fun past() = Instant.now().minusSeconds(10)

    private fun future() = Instant.now().plusSeconds(10)
  }

  companion object {
    private const val NOT_EMPTY = "NotEmpty"
    private const val FIELD_NAME = "fieldName"
  }
}
