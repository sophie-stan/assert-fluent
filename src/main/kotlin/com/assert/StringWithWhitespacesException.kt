package com.assert

class StringWithWhitespacesException(field: String) :
  AssertionException("The field \"$field\" contains at least one space")
