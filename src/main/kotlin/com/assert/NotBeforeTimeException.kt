package com.assert

import java.time.Instant

class NotBeforeTimeException private constructor(message: String) : AssertionException(message) {
  companion object {
    fun notStrictlyBefore(fieldName: String, other: Instant) =
      NotBeforeTimeException("Time in \"$fieldName\" must be strictly before $other but wasn't")

    fun notBefore(fieldName: String, other: Instant) =
      NotBeforeTimeException("Time in \"$fieldName\" must be before $other but wasn't")
  }
}
