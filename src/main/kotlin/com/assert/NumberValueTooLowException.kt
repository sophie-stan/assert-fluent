package com.assert

class NumberValueTooLowException(field: String, minValue: String, value: String) :
  AssertionException("Value of field \"$field\" must be at least $minValue but was $value") {
}
