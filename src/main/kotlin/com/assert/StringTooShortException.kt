package com.assert

class StringTooShortException(field: String, minLength: Int, value: String) :
  AssertionException("The value in field \"$field\" must be at least $minLength long but was only ${value.length}")
