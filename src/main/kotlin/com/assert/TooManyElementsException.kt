package com.assert

class TooManyElementsException(field: String, maxSize: Int, size: Int) :
  AssertionException("Size of collection \"$field\" must be at most $maxSize but was $size") {
}
