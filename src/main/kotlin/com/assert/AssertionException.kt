package com.assert

open class AssertionException(message: String) : RuntimeException(message)
