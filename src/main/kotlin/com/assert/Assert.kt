package com.assert

import java.math.BigDecimal
import java.time.Instant

/**
 * This class provides utilities for input assertions.
 *
 *
 *
 * It is designed to validate domain input, if you want to validate application input it's better to stick to
 * BeanValidation to get all errors at once and internationalized error messages.
 *
 *
 *
 *
 * The main goal of this class is to ensure some basic type validation in your classes. If you have to do business
 * related validation you should create your own exception and code dedicated to that check
 *
 */
object Assert {

  /**
   * Ensure that the value is not blank (empty or only whitespace)
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          input to check
   * @throws MissingMandatoryValueException
   *           if the input is blank
   */
  fun notBlank(field: String, input: String) {
    field(field, input).notBlank()
  }

  /**
   * Ensure that the given collection is not empty
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param collection
   *          collection to check
   * @throws MissingMandatoryValueException
   *           if the collection is empty
   */
  fun notEmpty(field: String, collection: Collection<*>) {
    field(field, collection).notEmpty()
  }

  /**
   * Ensure that the value contains no whitespace
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          input to check
   * @throws MissingMandatoryValueException
   *           if the input contains whitespace
   */
  fun noWhitespace(field: String, input: String) {
    field(field, input).noWhitespace()
  }

  /**
   * Create a fluent asserter for {@link String}
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("name", name)
   *   .notBlank()
   *   .maxLength(150);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          string to check
   * @return A {@link StringAsserter} for this field and value
   */
  fun field(field: String, input: String) = StringAsserter(field, input)

  /**
   * Create a fluent asserter for Integer values (int and {@link Integer})
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("age", age)
   *   .min(0)
   *   .max(150);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          value to check
   * @return An {@link IntegerAsserter} for this field and value
   */
  fun field(field: String, input: Int) = IntegerAsserter(field, input)

  /**
   * Create a fluent asserter for Long values (long and {@link Long})
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("duration", duration)
   *   .min(100)
   *   .max(500_000);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          value to check
   * @return An {@link LongAsserter} for this field and value
   */
  fun field(field: String, input: Long) = LongAsserter(field, input)

  /**
   * Create a fluent asserter for Float values (float and {@link Float})
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("rate", rate)
   *   .min(0)
   *   .max(1);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          value to check
   * @return An {@link DoubleAsserter} for this field and value
   */
  fun field(field: String, input: Float) = FloatAsserter(field, input)

  /**
   * Create a fluent asserter for Double values (double and {@link Double})
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("rate", rate)
   *   .min(0)
   *   .max(1);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          value to check
   * @return An {@link DoubleAsserter} for this field and value
   */
  fun field(field: String, input: Double) = DoubleAsserter(field, input)

  /**
   * Create a fluent asserter for {@link BigDecimal} values
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("rate", rate)
   *   .min(0)
   *   .max(1);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          value to check
   * @return An {@link BigDecimalAsserter} for this field and value
   */
  fun field(field: String, input: BigDecimal) = BigDecimalAsserter(field, input)

  /**
   * Create a fluent asserter for {@link Collection}
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("name", name)
   *  .notEmpty()
   *  .maxSize(150);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          collection to check
   * @return A {@link CollectionAsserter} for this field and value
   */
  fun <T> field(field: String, input: Collection<T>) = CollectionAsserter(field, input)

  /**
   * Create a fluent asserter for an Instant
   *
   * <p>
   * Usage:
   *
   * <code>
   * <pre>
   * Assert.field("date", date)
   *   .inPast()
   *   .after(otherDate);
   * </pre>
   * </code>
   * </p>
   *
   * @param field
   *          name of the field to check (will be displayed in exception message)
   * @param input
   *          value to check
   * @return An {@link InstantAsserter} for this field and value
   */
  fun field(field: String, input: Instant) = InstantAsserter(field, input)

  /**
   * Asserter dedicated to [String] assertions
   */
  class StringAsserter internal constructor(private val field: String, private val value: String) {

    /**
     * Ensure that the value is not blank (empty or only whitespace)
     *
     * @return The current asserter
     * @throws MissingMandatoryValueException
     *           if the value is blank
     */
    fun notBlank() = apply {
      if (value.isBlank()) {
        throw MissingMandatoryValueException.forBlankValue(field)
      }
    }

    /**
     * Ensure  that the value does not contain whitespace
     *
     * @return The current asserter
     * @throws MissingMandatoryValueException
     *           if the value contains whitespace
     */
    fun noWhitespace() = apply {
      if (value.contains(WHITE_SPACE)) {
        throw StringWithWhitespacesException(field)
      }
    }

    /**
     * Ensure that the input value is at least of the given length
     *
     * @param length
     *          inclusive min length of the [String]
     *
     * @return The current asserter
     * @throws StringTooShortException
     *           if the value is shorter than min length
     */
    fun minLength(length: Int) = apply {
      if (value.length < length) {
        throw StringTooShortException(field = field, value = value, minLength = length)
      }
    }

    /**
     * Ensure that the given input value is not over the given length
     *
     * @param length
     *          inclusive max length of the [String]
     * @return The current asserter
     * @throws StringTooLongException
     *           if the value is longer than the max length
     */
    fun maxLength(length: Int) = apply {
      if (value.length > length) {
        throw StringTooLongException(field = field, value = value, maxLength = length)
      }
    }

    companion object {
      private const val WHITE_SPACE = " "
    }
  }

  /**
   * Asserter dedicated to Integer values (int and [Integer])
   */
  class IntegerAsserter internal constructor(private val field: String, private val value: Int) {
    /**
     * Ensure that the input value is positive (0 is positive)
     *
     * @return The current asserters
     * @throws NumberValueTooLowException
     *           if the value is negative
     */
    fun positive(): IntegerAsserter = min(0)

    /**
     * Ensure that the input value is over the given value
     *
     * @param minValue
     *          inclusive min value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the value is under min
     */
    fun min(minValue: Int) = apply {
      if (value < minValue) {
        throw NumberValueTooLowException(field = field, minValue = minValue.toString(), value = value.toString())
      }
    }

    /**
     * Ensure that the input value is under the given value
     *
     * @param maxValue
     *          inclusive max value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is over max
     */
    fun max(maxValue: Int) = apply {
      if (value > maxValue) {
        throw NumberValueTooHighException(field = field, maxValue = maxValue.toString(), value = value.toString())
      }
    }
  }

  /**
   * Asserter dedicated to long values (long and [Long])
   */
  class LongAsserter internal constructor(private val field: String, private val value: Long) {
    /**
     * Ensure that the input value is positive (0 is positive)
     *
     * @return The current asserters
     * @throws NumberValueTooLowException
     *           if the value is negative
     */
    fun positive(): LongAsserter = min(0)

    /**
     * Ensure that the input value is over the given value
     *
     * @param minValue
     *          inclusive min value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the value is under min
     */
    fun min(minValue: Long) = apply {
      if (value < minValue) {
        throw NumberValueTooLowException(field = field, minValue = minValue.toString(), value = value.toString())
      }
    }

    /**
     * Ensure that the input value is under the given value
     *
     * @param maxValue
     *          inclusive max value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is over max
     */
    fun max(maxValue: Long) = apply {
      if (value > maxValue) {
        throw throw NumberValueTooHighException(field = field, maxValue = maxValue.toString(), value = value.toString())
      }
    }
  }

  /**
   * Asserter dedicated to float values (float and [Float])
   */
  class FloatAsserter internal constructor(private val field: String, private val value: Float) {
    /**
     * Ensure that the input value is positive (0 is positive)
     *
     * @return The current asserters
     * @throws NumberValueTooLowException
     *           if the value is negative
     */
    fun positive(): FloatAsserter = min(0f)

    /**
     * Ensure that the input value is strictly positive (0 is not strictly positive)
     *
     * @return The current asserters
     * @throws NumberValueTooLowException
     *           if the value is negative
     */
    fun strictlyPositive(): FloatAsserter = over(0f)

    /**
     * Ensure that the input value is over the given value
     *
     * @param minValue
     *          inclusive min value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the value is under min
     */
    fun min(minValue: Float) = apply {
      if (value < minValue) {
        throw tooLow(minValue)
      }
    }

    /**
     * Ensure that the input value is over the given floor
     *
     * @param floor
     *          exclusive floor value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is under floor
     */
    fun over(floor: Float) = apply {
      if (value <= floor) {
        throw tooLow(floor)
      }
    }

    private fun tooLow(floor: Float) =
      NumberValueTooLowException(field = field, minValue = floor.toString(), value = value.toString())

    /**
     * Ensure that the input value is under the given value
     *
     * @param maxValue
     *          inclusive max value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is over max
     */
    fun max(maxValue: Float) = apply {
      if (value > maxValue) {
        throw tooHigh(maxValue)
      }
    }

    /**
     * Ensure that the input value is under the given ceil
     *
     * @param ceil
     *          exclusive ceil value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is over ceil
     */
    fun under(ceil: Float) = apply {
      if (value >= ceil) {
        throw tooHigh(ceil)
      }
    }

    private fun tooHigh(ceil: Float) =
      NumberValueTooHighException(field = field, maxValue = ceil.toString(), value = value.toString())
  }

  /**
   * Asserter dedicated to double values (double and [Double])
   */
  class DoubleAsserter internal constructor(private val field: String, private val value: Double) {
    /**
     * Ensure that the input value is positive (0 is positive)
     *
     * @return The current asserters
     * @throws NumberValueTooLowException
     *           if the value is negative
     */
    fun positive(): DoubleAsserter = min(0.0)

    /**
     * Ensure that the input value is strictly positive (0 is not strictly positive)
     *
     * @return The current asserters
     * @throws NumberValueTooLowException
     *           if the value is negative
     */
    fun strictlyPositive(): DoubleAsserter = over(0.0)

    /**
     * Ensure that the input value is over the given value
     *
     * @param minValue
     *          inclusive min value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the value is under min
     */
    fun min(minValue: Double) = apply {
      if (value < minValue) {
        throw tooLow(minValue)
      }
    }

    /**
     * Ensure that the input value is over the given floor
     *
     * @param floor
     *          exclusive floor value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is under floor
     */
    fun over(floor: Double) = apply {
      if (value <= floor) {
        throw tooLow(floor)
      }
    }

    private fun tooLow(floor: Double) =
      NumberValueTooLowException(field = field, minValue = floor.toString(), value = value.toString())

    /**
     * Ensure that the input value is under the given value
     *
     * @param maxValue
     *          inclusive max value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is over max
     */
    fun max(maxValue: Double) = apply {
      if (value > maxValue) {
        throw tooHigh(maxValue)
      }
    }

    /**
     * Ensure that the input value is under the given ceil
     *
     * @param ceil
     *          exclusive ceil value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is over ceil
     */
    fun under(ceil: Double) = apply {
      if (value >= ceil) {
        throw tooHigh(ceil)
      }
    }

    private fun tooHigh(ceil: Double) =
      NumberValueTooHighException(field = field, maxValue = ceil.toString(), value = value.toString())
  }

  /**
   * Asserter dedicated to [BigDecimal] assertions
   */
  class BigDecimalAsserter internal constructor(private val field: String, private val value: BigDecimal) {
    /**
     * Ensure that the input value is positive (0 is positive)
     *
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the input value is negative
     */
    fun positive(): BigDecimalAsserter = min(0)

    /**
     * Ensure that the input value is strictly positive (0 is not strictly positive)
     *
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the input value is negative
     */
    fun strictlyPositive(): BigDecimalAsserter = over(0)

    /**
     * Ensure that the input value is at least at min value
     *
     * @param minValue
     *          inclusive min value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the input value is under the min value
     */
    fun min(minValue: Long): BigDecimalAsserter = min(BigDecimal(minValue))

    /**
     * Ensure that the input value is at least at min value
     *
     * @param minValue
     *          inclusive min value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the input value is under the min value
     */
    fun min(minValue: BigDecimal) = apply {
      if (value < minValue) {
        throw tooLow(minValue)
      }
    }

    /**
     * Ensure that the input value is over the given floor
     *
     * @param floor
     *          exclusive floor value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the value is under floor
     */
    fun over(floor: Long): BigDecimalAsserter = over(BigDecimal(floor))

    /**
     * Ensure that the input value is over the given floor
     *
     * @param floor
     *          exclusive floor value
     * @return The current asserter
     * @throws NumberValueTooLowException
     *           if the value is under floor
     */
    fun over(floor: BigDecimal) = apply {
      if (value <= floor) {
        throw tooLow(floor)
      }
    }

    private fun tooLow(floor: BigDecimal) =
      NumberValueTooLowException(field = field, minValue = floor.toString(), value = value.toString())

    /**
     * Ensure that the input value is at most at max value
     *
     * @param maxValue
     *          inclusive max value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the input value is over max
     */
    fun max(maxValue: Long): BigDecimalAsserter = max(BigDecimal(maxValue))

    /**
     * Ensure that the input value is at most at max value
     *
     * @param maxValue
     *          inclusive max value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the input value is over max
     */
    fun max(maxValue: BigDecimal) = apply {
      if (value > maxValue) {
        throw tooHigh(maxValue)
      }
    }

    /**
     * Ensure that the input value is under the given ceil
     *
     * @param ceil
     *          exclusive ceil value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is under floor
     */
    fun under(ceil: Long): BigDecimalAsserter = under(BigDecimal(ceil))

    /**
     * Ensure that the input value is under the given ceil
     *
     * @param ceil
     *          exclusive ceil value
     * @return The current asserter
     * @throws NumberValueTooHighException
     *           if the value is under floor
     */
    fun under(ceil: BigDecimal) = apply {
      if (value >= ceil) {
        throw tooHigh(ceil)
      }
    }

    private fun tooHigh(ceil: BigDecimal) =
      NumberValueTooHighException(field = field, maxValue = ceil.toString(), value = value.toString())
  }

  /**
   * Asserter dedicated to [Collection] assertions
   */
  class CollectionAsserter<T> internal constructor(private val field: String, value: Collection<T>) {
    private val value: Collection<T>

    init {
      this.value = value
    }

    /**
     * Ensure that the value is not empty
     *
     * @return The current asserter
     * @throws MissingMandatoryValueException
     *           if the value is empty
     */
    fun notEmpty() = apply {
      if (value.isEmpty()) {
        throw MissingMandatoryValueException.forEmptyValue(field)
      }
    }

    /**
     * Ensure that the size of the given input value is not over the given size
     *
     * @param maxSize
     *          inclusive max size of the [Collection]
     * @return The current asserter
     * @throws TooManyElementsException
     *           if the size of value is over the max size
     */
    fun maxSize(maxSize: Int) = apply {
      if (value.size > maxSize) {
        throw TooManyElementsException(field = field, maxSize = maxSize, size = value.size)
      }
    }
  }

  /**
   * Asserter dedicated to instant value
   */
  class InstantAsserter internal constructor(private val field: String, private val value: Instant) {
    /**
     * Ensure that the given instant is in the future or at current Instant (considering this method invocation time)
     *
     * @return The current asserter
     * @throws NotAfterTimeException
     *           if the input instant is in past
     */
    fun inFuture(): InstantAsserter = afterOrAt(Instant.now())

    /**
     * Ensure that the input instant is after the given instant
     *
     * @param other
     *          exclusive after instant
     * @return The current asserter
     * @throws NotAfterTimeException
     *           if the input instant is not after the other instant
     */
    fun after(other: Instant) = apply {
      if (value <= other) {
        throw NotAfterTimeException.notStrictlyAfter(field, other)
      }
    }

    /**
     * Ensure that the input instant is after the given instant
     *
     * @param other
     *          inclusive after instant
     * @return The current asserter
     * @throws NotAfterTimeException
     *           if the input instant is not after the other instant
     */
    fun afterOrAt(other: Instant) = apply {
      if (value < other) {
        throw NotAfterTimeException.notAfter(field, other)
      }
    }

    /**
     * Ensure that the given instant is in the past or at current Instant (considering this method invocation time)
     *
     * @return The current asserter
     * @throws NotBeforeTimeException
     *           if the input instant is in future
     */
    fun inPast(): InstantAsserter = beforeOrAt(Instant.now())

    /**
     * Ensure that the input instant is before the given instant
     *
     * @param other
     *          exclusive before instant
     * @return The current asserter
     * @throws NotBeforeTimeException
     *           if the input instant is not before the other instant
     */
    fun before(other: Instant) = apply {
      if (value >= other) {
        throw NotBeforeTimeException.notStrictlyBefore(field, other)
      }
    }

    /**
     * Ensure that the input instant is before the given instant
     *
     * @param other
     *          inclusive before instant
     * @return The current asserter
     * @throws NotBeforeTimeException
     *           if the input instant is not before the other instant
     */
    fun beforeOrAt(other: Instant) = apply {
      if (value > other) {
        throw NotBeforeTimeException.notBefore(field, other)
      }
    }
  }
}
