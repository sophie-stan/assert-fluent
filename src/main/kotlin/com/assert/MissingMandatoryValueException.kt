package com.assert

class MissingMandatoryValueException private constructor(message: String) : AssertionException(message) {
  companion object {
    fun forBlankValue(field: String) = MissingMandatoryValueException(defaultMessage(field, "blank"))

    fun forEmptyValue(field: String) = MissingMandatoryValueException(defaultMessage(field, "empty"))

    private fun defaultMessage(field: String, reason: String) =
      "The field \"$field\" is mandatory and wasn't set ($reason)"
  }
}
