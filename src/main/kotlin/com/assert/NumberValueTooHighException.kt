package com.assert

class NumberValueTooHighException(field: String, maxValue: String, value: String) :
  AssertionException("Value of field \"$field\" must be at most $maxValue but was $value")
