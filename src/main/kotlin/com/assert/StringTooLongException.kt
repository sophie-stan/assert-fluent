package com.assert

class StringTooLongException(field: String, maxLength: Int, value: String) :
  AssertionException("The value in field \"$field\" must be at most $maxLength long but was ${value.length}")
