package com.assert

import java.time.Instant

class NotAfterTimeException private constructor(message: String) : AssertionException(message) {
  companion object {
    fun notStrictlyAfter(fieldName: String, other: Instant) =
      NotAfterTimeException("Time in \"$fieldName\" must be strictly after $other but wasn't")

    fun notAfter(fieldName: String, other: Instant) =
      NotAfterTimeException("Time in \"$fieldName\" must be after $other but wasn't")
  }
}
